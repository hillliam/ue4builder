﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace ue4builder
{
    public partial class Form1 : Form
    {

        enum target
        {
            UE4Game,
            UE4Client,
            UE4Editor,
            UE4Server,
            BlankProgram,
            CrashReportClient,
            MinidumpDiagnostic,
            ShaderCompileWorker,
            SlateViewer,
            SymbolDebugger,
            UE4EditorServices,
            UnrealFileServer,
            UnrealFrontend,
            UnrealHeaderTool,
            UnrealLaunchDaemon,
            UnrealLightmass,
            UnrealPak,
            UnrealVersionSelector,
            BootstrapPackagedGame,
            BuildPatchTool,
            CrossCompilerTool,
            DsymExporter,
            ParallelExecutor,
            ShaderCacheTool,
            TestPAL,
            UnrealAtoS,
            UnrealCEFSubProcess,
            UnrealCodeAnalyzer,
            UnrealSync,
            UnrealTournamentEditor
        }//29
        enum platform
        {
            Win32,
            Win64,
            WinRT,
            WinRT_ARM,
            UWP,
            Mac,
            XboxOne,
            PS4,
            IOS,
            Android,
            HTML5,
            Linux,
            AllDesktop,
            TVOS,
        }//14
        enum specification
        {
            Test,
            Debug,
            DebugGame,
            Development,
            Shipping
        }//5
        public string builder = @"Engine\Build\BatchFiles\Build.bat";
        public Form1()
        {
            InitializeComponent();
            if (File.Exists(Path.Combine(Environment.CurrentDirectory,builder)))
            {
                textBox1.Text = Environment.CurrentDirectory;
            }
            checkedListBox1.Items.AddRange((object[])Enum.GetNames(typeof(target)));
            checkedListBox2.Items.AddRange((object[])Enum.GetNames(typeof(platform)));
            checkedListBox3.Items.AddRange((object[])Enum.GetNames(typeof(specification)));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog a = new FolderBrowserDialog();
            if (a.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = a.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int a = 0; a < checkedListBox3.Items.Count; a++)//specification
            {
                if (!checkedListBox3.GetItemChecked(a))
                    continue;
                for (int b = 0; b < checkedListBox2.Items.Count; b++)//platform
                {
                    if (!checkedListBox2.GetItemChecked(b))
                        continue;
                    for (int c = 0; c < checkedListBox1.Items.Count; c++)//target
                    {
                        if (!checkedListBox1.GetItemChecked(c))
                            continue;
                        string option = Enum.GetName(typeof(target), c) + " " + Enum.GetName(typeof(platform), b) + " " + Enum.GetName(typeof(specification), a);
                        bool d = startcompile(Path.Combine(textBox1.Text, builder), textBox1.Text, option, checkBox1.Checked);
                        listBox1.Items.Add(option + " " + a.ToString());
                    }
                }
            }
        }
        public bool startcompile(string buildprogram, string starting, string options, bool filesave)
        {
            ProcessStartInfo a = new ProcessStartInfo(buildprogram, options);
            a.WorkingDirectory = starting;
            a.UseShellExecute = false;
            a.RedirectStandardError = filesave;
            a.RedirectStandardOutput = filesave;
            Process b = Process.Start(a);
            b.WaitForExit();
            if (filesave)
            {
                string v = b.StandardOutput.ReadToEnd();
                File.WriteAllText(Path.Combine(Environment.CurrentDirectory, options + ".log"), v);
            }
            if (b.ExitCode != 0)
            {
                return false;
            }
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 14; j++)
                {
                    for (int c = 0; c < 29; c++)
                    {
                        string option = Enum.GetName(typeof(target),c) + " " + Enum.GetName(typeof(platform),j) + " " + Enum.GetName(typeof(specification),i);
                        bool a = startcompile(Path.Combine(textBox1.Text,builder), textBox1.Text, option, checkBox1.Checked);
                        listBox1.Items.Add(option + " " + a.ToString());
                    }
                }
            }
        }
    }
}
